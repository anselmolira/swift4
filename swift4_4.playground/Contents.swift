//: Swift 4 (Intermediário) - Loops e funções

import UIKit

// Resumão do que foi visto até agora
// Variável (com tipo embutido)
var helloTxt = "Hello, playground"
// Constante (com tipo definido)
let name: String = "Anselmo"
var age:Int = 31

// Exibindo um conteúdo
print("Name = " + name + " | Age = " + String(age))

// Declarando array do tipo String
var fruits: [String] = []
// Adicionando elemento ao array
fruits.append("Apple")
fruits.append("Pineapple")
fruits.append("Pear")
fruits.append("Strawberry")

// Removendo elemento do array
fruits.remove(at: 2)
print(fruits)

// Set
var list = Set<String>()
list.insert("pasta")
list.insert("juice")
list.insert("juice") // não será inserido
list.remove("juice") // Removendo um ítem pelo nome
print(list)

// Dicts
var animals = [String:String]()
// Adicionando chaves/valores
animals["dog"] = "Like as Scooby."
animals["cat"] = "Like as Tom."
print(animals["cat"]!)

/*
 Operadores básicos aritméticos:
 
 1 - Somar (+)
 2 - Subtrair (-)
 3 - Multiplicar (*)
 4 - Dividir (/)
 */

var no1: Int = 12
var no2: Int = 3

var addOp: Int = no1 + no2
var subOp: Int = no1 - no2
var multOp: Int = no1 * no2
var divOp: Int = no1 / no2

/*
 Operadores relacionais:
 
 1 - Logicamente igual (==)
 2 - Logicamente diferente (!=)
 3 - Maior que (>)
 4 - Menor que (<)
 5 - Maior ou igual a (>=)
 6 - Menor ou igual a (<=)
 
 Operadores lógicos:
 
 1 - e (&&)
 2 - ou (||)
 */

// Usando os operadores relacionais
no2 = 12
no1 >= no2
no1 == no2


// Usando os operadores lógicos
var precoCarro: Int = 100
precoCarro > 50 && precoCarro < 150

precoCarro = 200
precoCarro > 50 || precoCarro < 150

// Uso de condicionais (if/else)
// Parênteses são opcionais aqui (podem ser utilizados ou não)
var idade: Int = 30
if idade > 18
{
    print("\nA pessoa é maior de idade\n");
}
else if(idade > 13 && idade < 18)
{
    print("\nTemos um adolescente aqui\n")
}
else
{
    print("\nÉ apenas uma criança\n");
}


// Loops (for e while)
// For
for var i in 1..<6
{
    print("i = " + String(i))
}

var comments: [String] = []
comments.append("Gostei da foto")
comments.append("Que legal a sua viagem!")
comments.append("Ficou muito bonita nessa foto")

for var i in 0..<comments.count
{
    print("\nComentário " + String(i) + ": " + comments[i] + "\n")
}

// While
var counter: Int = 0
var newCounter: Int = 0
while counter < 6
{
    // Incremento do contador manual (como em outras linguagens)
    counter += 1
    // counter = counter + 1
}


// Repeat
repeat
{
    print("newCounter = " + String(newCounter))
    newCounter += 1
} while newCounter < 6



// Funções
// Definição de função: func funcName(paramName: type, .....) -> returnType
func deltaCalc(coef_a: Int, coef_b: Int, coef_c: Int) -> Int
{
    let delta: Int = (coef_b^2) - 4 * coef_a * coef_c
    return delta
}

var delta_value: Int = deltaCalc(coef_a: 1, coef_b: 2, coef_c: 3)
print("\nValor de delta: " + String(delta_value))

// Retorna um valor aleatório
arc4random_uniform(20)


// Desafio aula 26
func ageCalc(yearBirth: Int) -> Int
{
    let currentYear: Int = 2018
    let age: Int = currentYear - yearBirth
    return age
}

var currentAge: Int = ageCalc(yearBirth: 1986)


