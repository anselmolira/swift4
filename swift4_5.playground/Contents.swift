//: Swift 4 (Intermediário) - Opcionais e OO

import UIKit

// Declarando e definindo variável
var year: Int = 2018
var birthYear: Int?
var ageTemp: Int = 0

// Opcionais: aceitam o valor nulo (nil)
/* Opcionais devem ser utilizados com o ponto de exclamação.
   O ponto de exclamação informa ao compilador do swift que estou garantindo que o
   opcional possui um valor
 */

// Testando se birthYear possui valor atribuído (é assim que se testa, acredite)
birthYear = 1986
if let testedBirthYear = birthYear
{
    ageTemp = year - birthYear!
}

print("ageTemp = " + String(ageTemp))



// Orientação a Objetos

class Casa
{
    // Atributos
    var cor: String
    var altura: Float
    
    // Métodos
    func getCor() -> String { return self.cor; }
    func setCor(nomeCor: String) { self.cor = nomeCor; }
    
    func getAltura() -> Float { return self.altura; }
    func setAltura(valorAltura: Float) { self.altura = valorAltura; }
    
    init(cor: String, altura: Float)
    {
        self.cor = cor
        self.altura = altura
    }
}

// Criando o objeto
var casa = Casa(cor: "Azul", altura: 2.5)
casa.getCor()


// Desafio lição 28
class Cachorro
{
    init(nome: String, cor: String)
    {
        self.nome = nome;
        self.cor = cor;
    }
    
    var nome: String
    func getNome() -> String { return self.nome; }
    func setNome(nome: String) { self.nome = nome; }
    
    var cor: String
    func getCor() -> String { return self.cor; }
    func setCor(nomeCor: String) { self.cor = nomeCor; }
    
    func correr()
    {
        print(self.nome + " está correndo.")
    }
    
    func latir()
    {
        print(self.nome + " está latindo.")
    }
}

var cachorro = Cachorro(nome: "Scooby", cor: "bege")
cachorro.correr();
cachorro.latir();




