// Swift Iniciante: Variáveis e arrays

import UIKit

/*
   Uso de "var" para definir.
   Tipo é embutido mas pode ser definido (declaração da variável)
 */

var userName: String = "Anselmo"
userName = "Luan"

// Int
var age: Int = 31

// Posso apenas declarar uma variável sem ter que definí-la

// Double: precisão de 15 casas decimais
var currency: Double
currency = 9.4

// Float: precisão de apenas 6 casas decimais
var pi: Float
pi = 3.1416

// Printando o valor (typecast para String)
print("O valor da idade é " + String(age) + " com o custo de " + String(currency))


// Desafio aula 15
var value1 = 3 // Int
var value2: Float = 12.5 // Float
var result: Float = Float(value1) + value2 // Typecast de int pra float
print("O resultado da soma é " + String(result))


// Arrays
var fruitList = ["Apple", "Orange", "Pineapple", "Strawberry"]
fruitList[1]

// Definindo um array por tipo (no caso, String)
var names: [String]

names = ["Anselmo"]
// Adicionando elemento com o operador +
names += ["Luan"]
// Adicionando com o método append
names.append("Raphael")
print(names)

// Removendo ítem pelo índice
names.remove(at: 1)
print(names)


// Desafio aula 16
// Array deve ser inicializado antes de ser utilizado (tipo um new)
var frases: [String] = []
frases.append("A vingança nunca é plena, mata a alma e a envenena.")
frases.append("Ninguém tem paciência comigo")
frases.append("Não contaram com a minha astúcia")

print("\n\n")
print("Frase 1: " + frases[0])
print("Frase 2: " + frases[1])
print("Frase 3: " + frases[2])
