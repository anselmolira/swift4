//: Swift 4 -> Sets & Dicts

import UIKit

// Resumão do que foi visto até agora
// Variável (com tipo embutido)
var helloTxt = "Hello, playground"
// Constante (com tipo definido)
let name: String = "Anselmo"
var age:Int = 31

// Exibindo um conteúdo
print("Name = " + name + " | Age = " + String(age))

// Declarando array do tipo String
var fruits: [String] = []
// Adicionando elemento ao array
fruits.append("Apple")
fruits.append("Pineapple")
fruits.append("Pear")
fruits.append("Strawberry")

// Removendo elemento do array
fruits.remove(at: 2)
print(fruits)


// Sets
/*
 1 - Não há preocupação com a ordem
 2 - Ítens distintos (valores não se repetem)
 
 OBS: se eu tentar inserir valor que já existe, ele não será inserido
 */
var list = Set<String>()
list.insert("pasta")
list.insert("juice")
list.insert("juice") // não será inserido

// Aqui serão exibidos somente 2 ítens
print(list)




// Dicts
/*
 Mesmo conceito das outras linguagens: chave/valor.
 O tipo da chave deve ser o mesmo do declarado.
 O tipo do valor deve ser o mesmo do declarado.
 */

// Declarando um dict
var animals = [String:String]()
// Adicionando chaves/valores
animals["dog"] = "Like as Scooby."
animals["cat"] = "Like as Tom."

/*
 Uso do ponto de exclamação: como o Swift não pode assegurar que a chave exista, ele exibe o alerta de Optional. Quando fornecemos o ponto de exclamação, estamos assegurando a ele que o ítem da chave dada existe.
 
 ! = afirma que o ítem de chave fornecida existe
 */
print(animals["cat"]!)


// Desafio 1 - aula 20
var months = [Int:String]()
months[1] = "january"
months[2] = "february"
months[3] = "march"
months[4] = "april"
months[5] = "may"
months[6] = "june"
months[7] = "july"
months[8] = "august"
months[9] = "september"
months[10] = "october"
months[11] = "november"
months[12] = "december"

print("\n")
print("Mês 1: " + months[1]!)
print("Mês 2: " + months[2]!)

