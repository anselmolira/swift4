//: Swift 4 - Operadores e Condicionais

import UIKit

// Resumão do que foi visto até agora
// Variável (com tipo embutido)
var helloTxt = "Hello, playground"
// Constante (com tipo definido)
let name: String = "Anselmo"
var age:Int = 31

// Exibindo um conteúdo
print("Name = " + name + " | Age = " + String(age))

// Declarando array do tipo String
var fruits: [String] = []
// Adicionando elemento ao array
fruits.append("Apple")
fruits.append("Pineapple")
fruits.append("Pear")
fruits.append("Strawberry")

// Removendo elemento do array
fruits.remove(at: 2)
print(fruits)

// Set
var list = Set<String>()
list.insert("pasta")
list.insert("juice")
list.insert("juice") // não será inserido
list.remove("juice") // Removendo um ítem pelo nome
print(list)

// Dicts
var animals = [String:String]()
// Adicionando chaves/valores
animals["dog"] = "Like as Scooby."
animals["cat"] = "Like as Tom."
print(animals["cat"]!)


/*
 Operadores básicos aritméticos:
 
    1 - Somar (+)
    2 - Subtrair (-)
    3 - Multiplicar (*)
    4 - Dividir (/)
 */

var no1: Int
var no2: Int

no1 = 12
no2 = 3

var addOp: Int = no1 + no2
var subOp: Int = no1 - no2
var multOp: Int = no1 * no2
var divOp: Int = no1 / no2


/*
 Operadores relacionais:
 
    1 - Logicamente igual (==)
    2 - Logicamente diferente (!=)
    3 - Maior que (>)
    4 - Menor que (<)
    5 - Maior ou igual a (>=)
    6 - Menor ou igual a (<=)
 
 Operadores lógicos:
 
    1 - e (&&)
    2 - ou (||)
 */

// Usando os operadores relacionais
no2 = 12
no1 >= no2
no1 == no2


// Usando os operadores lógicos
var precoCarro: Int = 100
precoCarro > 50 && precoCarro < 150

precoCarro = 200
precoCarro > 50 || precoCarro < 150



// Desafio Aula 22 (seção 3)
var idade: Int = 11
idade >= 18 && idade <= 26



// Uso de condicionais (if/else)
// Parênteses são opcionais aqui (podem ser utilizados ou não)
if idade > 18
{
    print("\nA pessoa é maior de idade\n");
}
else if(idade > 13 && idade < 18)
{
    print("\nTemos um adolescente aqui\n")
}
else
{
    print("\nÉ apenas uma criança\n");
}



// Desafio Aula 23 (seção 3)
var userAge: Int
userAge = 17
var message: String

if(userAge < 18)
{
    message = "Acesso não permitido: exclusivo para maiores de idade."
}
else
{
    message = "Acesso liberado"
}

print ("Resultado do teste de acesso: " + message)



